<?php
require_once __DIR__ . '/../vendor/autoload.php';

use PHPUnit\Framework\TestCase;

class SubmitTest extends TestCase
{
    public function testSubmit()
    {
        // Simulate a POST request to submit.php with the name 'John Doe'
        $_POST['name'] = 'John Doe';
        $_SERVER['REQUEST_METHOD'] = 'POST';

        // Start output buffering to capture output
        ob_start();
        include __DIR__ . '/../submit.php';
        $output = ob_get_clean();

        // Check that the output contains the expected text
        $this->assertStringContainsString('Hello, John Doe! Your data has been saved.', $output);

        // Check that the data was saved in data.txt
        $this->assertFileExists(__DIR__ . '/../data.txt');
        $this->assertStringContainsString('John Doe', file_get_contents(__DIR__ . '/../data.txt'));

        // Clean up
        unset($_POST['name']);
        unset($_SERVER['REQUEST_METHOD']);
    }
}
