<?php
require_once __DIR__ . '/../vendor/autoload.php';

use PHPUnit\Framework\TestCase;



class IndexTest extends TestCase
{
    public function testIndex()
    {
        $output = file_get_contents('index.php');
        $this->assertStringContainsString('Hello World, let\'s start for today', $output);
    }
}
