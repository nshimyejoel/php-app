<?php
require_once __DIR__ . '/../vendor/autoload.php';

use PHPUnit\Framework\TestCase;

class FormTest extends TestCase
{
    public function testFormPresence()
    {
        $output = file_get_contents('index.php');

        // Check if the form element is present
        $this->assertStringContainsString('<form action="submit.php" method="post">', $output);

        // Check if the name input is present and required
        $this->assertStringContainsString('<input type="text" id="name" name="name" required>', $output);

        // Check if the submit button is present
        $this->assertStringContainsString('<input type="submit" value="Submit">', $output);
    }
}
