<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $name = htmlspecialchars($_POST['name']);
    file_put_contents("data.txt", $name . PHP_EOL, FILE_APPEND);
    echo "Hello, " . $name . "! Your data has been saved.";
}
?>
<br>
<a href="index.php">Go back</a>